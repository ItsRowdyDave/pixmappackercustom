package com.towerdefense.pixmaxpackercustom;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.towerdefense.factorycollection.AbstractFactoriesCollection;
//import com.badlogic.gdx.tools.texturepacker.TexturePacker;

public class PixmapPackerCustom extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;
	Texture createImage;
	Texture tmpTex;
	boolean isCreated = false;
	AbstractFactoriesCollection abstractFactoriesCollection;


	private static final float SCENE_WIDTH = 400f;
	private static final float SCENE_HEIGHT = 400f;

	private static final float FBO_WIDTH = 200f;
	private static final float FBO_HEIGHT = 200f;

	int counter=1;

	private OrthographicCamera camera;
	private Viewport viewport;
	private Viewport viewportFULL;

	FrameBuffer frameBuffer;
	TextureRegion frameBufferTextureRegion = new TextureRegion();


	//to render all the shapes, mainly debug mode;
	private ShapeRenderer shapeRenderer;


	public PixmapPackerCustom(AbstractFactoriesCollection abstractFactoriesCollection){
        super();

		this.abstractFactoriesCollection=abstractFactoriesCollection;

        this.abstractFactoriesCollection.init();
        //abstractFactoriesCollection.abstractVecToRasterFactory.initializeVecToRasterFactory("xmlstore","xmlstore");

    }

	@Override
	public void create () {


		batch = new SpriteBatch();
		/*
		System.out.println("Reached");
		System.out.println(abstractFactoriesCollection==null);
		System.out.println(batch==null);

		//abstractFactoriesCollection.abstractFactoriesParameter.setBatch(batch);
		System.out.println("Reached2");

		abstractFactoriesCollection.init();
		System.out.println("Reached");
		*/
		img = new Texture("badlogic.jpg");
		//camera = new OrthographicCamera();//SCENE_WIDTH,SCENE_HEIGHT);

		abstractFactoriesCollection.abstractVecToRasterFactory.transformSVGtoPNG("arrow.png","xmlstore/arrow.svg");

		FileHandle cached = Gdx.files.external("arrow.png");

		img = new Texture(cached);

		/*
		//System.out.println("FirstEntry");
		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");

		//TexturePacker texturePacker;

		abstractFactoriesCollection.abstractVecToRasterFactory.initializeVecToRasterFactory("xmlstore","xmlstore");

        System.out.println("StartProcess");
        abstractFactoriesCollection.abstractVecToRasterFactory.transformSVGtoPNG("arrow.png","arrow.svg");
		//DesktopTransformerVecToRaster desktopTransformerVecToRaster = new DesktopTransformerVecToRaster("xmlstore","xmlstore");
		//desktopTransformerVecToRaster.transformSVGtoPNG("arrow.png","arrow.svg");

		//img = abstractFactoriesCollection.abstractVecToRasterFactory.createTexture();



		//FileHandle fileHandle = Gdx.files.local("pngstore/picture.png");
		//img = new Texture(fileHandle);
		*/
		/*
		img = new Texture("badlogic.jpg");
		camera = new OrthographicCamera();//SCENE_WIDTH,SCENE_HEIGHT);
		viewport = new FitViewport(SCENE_WIDTH, SCENE_HEIGHT, camera);

		viewportFULL = new StretchViewport(SCENE_WIDTH, SCENE_HEIGHT);
		//viewportFULL = new StretchViewport(FBO_WIDTH, FBO_HEIGHT);

		//camera2 = new OrthographicCamera(SCENE_WIDTH,SCENE_HEIGHT);

		camera.position.set(SCENE_WIDTH * 0.5f, SCENE_HEIGHT * 0.5f, 0.0f);
		//camera2.position.set(SCENE_WIDTH * 0.5f, SCENE_HEIGHT * 0.5f, 0.0f);


		shapeRenderer = new ShapeRenderer();
		frameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, (int)SCENE_WIDTH, (int)SCENE_HEIGHT, false);

		//createPNGfromTexture(img, 200, 200);
		*/
		shapeRenderer = new ShapeRenderer();

	}

	@Override
	public void render () {

		int pngWidth = 600;
		int pngHeight = 600;

		//Pixmap pixmap;
		//FrameBuffer tmpFrameBuffer;
		/*
		if (isCreated != true) {

			isCreated = true;

			createImage = abstractFactoriesCollection.abstractVecToRasterFactory.createTexture(pngWidth, pngHeight);

			//createImage = new Texture("badlogic.jpg");

		} //Comment this bracket for option 2

			FrameBuffer tmpFrameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, pngWidth, pngHeight, false);

			tmpFrameBuffer.bind();

			Gdx.gl.glClearColor(0, 0, 1, 1);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

			batch.begin();

			batch.draw(createImage, 0, 0);

			batch.end();

			byte[] pixels = ScreenUtils.getFrameBufferPixels(0, 0, pngWidth, pngHeight, true);

			Pixmap pixmap = new Pixmap(pngWidth, pngHeight, Pixmap.Format.RGBA8888);
			BufferUtils.copy(pixels, 0, pixmap.getPixels(), pixels.length);
			PixmapIO.writePNG(Gdx.files.external("storedimage.png"), pixmap);
			pixmap.dispose();


			tmpFrameBuffer.unbind();

			tmpFrameBuffer.dispose();

			img = new Texture(Gdx.files.external("storedimage.png"));

		// } uncomment this bracket for Option 2
		*/
			Gdx.gl.glClearColor(1, 0, 1, 1);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

			batch.begin();

			batch.draw(img, 400, 400);

			batch.end();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
	}

	/*
	@Override
	public void resize(int width,int height){

		//viewport.update(width, height); //here we give the screen dimensions but the viewport itself DOES not take the dimension width and height, the viewport dimensions are set according to width and height, but not TO width and height itself
		//viewportFULL.update(width, height);

		//camera.position.set(SCENE_WIDTH * 0.5f, SCENE_HEIGHT * 0.5f, 0.0f);
		//stage.getViewport().update(width, height, false);
		//stage.getViewport().setCamera(camera);
		//stage.getViewport().update(width, height, false);
		//stage.getViewport().update(width, height, true);
		//camera.update();


		//frameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, false);

	}
	*/

	//MUST be called in render
	public void createPNGfromTexture(Texture texture, int pngWidth, int pngHeight){


		Gdx.gl.glClearColor(0, 0, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		//batch.setProjectionMatrix(camera.combined);

		//FrameBuffer tmpFrameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, pngWidth, pngHeight, false);

		//tmpFrameBuffer.bind();

		batch.begin();
		batch.draw(texture,0,0);//,
		//0.0f, 0.0f,
		//600.0f, 600.0f);

		batch.end();
		batch.begin();

		shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
		shapeRenderer.setColor(Color.GREEN);

		shapeRenderer.circle(50f,50f,80);//getRangeCircle();//rect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);

		shapeRenderer.end();


		byte[] pixels = ScreenUtils.getFrameBufferPixels(0, 0, pngWidth, pngHeight, true);

		Pixmap pixmap = new Pixmap(pngWidth, pngHeight, Pixmap.Format.RGBA8888);
		BufferUtils.copy(pixels, 0, pixmap.getPixels(), pixels.length);
		PixmapIO.writePNG(Gdx.files.local("pngstore/mypixmap2.png"), pixmap);
		pixmap.dispose();



		batch.end();

		//tmpFrameBuffer.unbind();  //Interestingly here the Framebuffer.unbind is called and not lightEffectFrameBuffer.unbind();



		/*
		Gdx.gl.glViewport(0, 0, pngWidth, pngHeight); //Very Important! to set the glViewport to the correct status

		Gdx.gl.glClearColor(0, 0, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		Matrix4 normalProjection = new Matrix4().setToOrtho2D(0, 0, pngWidth, pngHeight); //the dimension of the actual shown screen (without the black bars)

		FrameBuffer tmpFrameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, pngWidth, pngHeight, false);
		Viewport viewportComplete = new StretchViewport(pngWidth, pngHeight); //we pick arbitrary worlddimensions, we just want to ensure that the texture fills the whole screen
		viewportComplete.update(pngWidth,pngHeight); //This gives the actual pixel size

		Gdx.gl.glViewport(0, 0, pngWidth, pngHeight); //Very Important! to set the glViewport to the correct status


		texture.getTextureData().prepare();
		Pixmap tmpPixmap;
		tmpPixmap = texture.getTextureData().consumePixmap();

		System.out.println("Width:" + tmpPixmap.getWidth());

		System.out.println("Height:" + tmpPixmap.getHeight());

		for(int i = 0 ; i<20;i++){


			System.out.println("Pixel "+i+": "+tmpPixmap.getPixel(i,i));

		}
		texture = new Texture(tmpPixmap);

		//Gdx.gl.glClearColor(0, 0, 1, 1);
		//Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		viewportComplete.apply();
		tmpFrameBuffer.bind();

		Gdx.gl.glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.setProjectionMatrix(normalProjection);

		batch.begin();


		batch.draw(texture,
				0.0f, 0.0f,
				pngWidth, pngHeight);


		batch.end();

		batch.begin();
		//shapeRenderer.setProjectionMatrix(camera.combined);
		shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
		shapeRenderer.setColor(Color.GREEN);

		shapeRenderer.circle(50f,50f,80);//getRangeCircle();//rect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);

		shapeRenderer.end();


		byte[] pixels = ScreenUtils.getFrameBufferPixels(0, 0, pngWidth, pngHeight, true);

		Pixmap pixmap = new Pixmap(pngWidth, pngHeight, Pixmap.Format.RGBA8888);
		BufferUtils.copy(pixels, 0, pixmap.getPixels(), pixels.length);
		PixmapIO.writePNG(Gdx.files.local("pngstore/mypixmap2.png"), pixmap);
		pixmap.dispose();


		batch.end();

		tmpFrameBuffer.unbind();  //Interestingly here the Framebuffer.unbind is called and not lightEffectFrameBuffer.unbind();

		tmpFrameBuffer.dispose();
		*/
	}


}
