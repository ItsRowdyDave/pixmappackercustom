package com.towerdefense.buttoncreator;

//import android.content.ContextWrapper;
//import android.content.ContextWrapper;//*;//content.Context;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

//import java.nio.file.Paths;
//import android.content.ContentWrapper;



/**
 * Created by Dave on 12/06/2017.
 */

public class TransformerVecToRaster {

    float iconWidth=40f;
    float iconHeight=40f;

    String localFileDirectoryTmp;
    String localFileDirectorySave;
    String localFileDirectorySVG;
    String localFileDirectoryTmpAbsolut;
    String localFileDirectorySaveAbsolut;
    String localFileDirectorySVGAbsolut;


    public TransformerVecToRaster(String localFileDirectorySVG,String localFileDirectorySave){
        //directoryPath = "C:/Users/Dave/Desktop/libGDX/FreeTypeFont/android/assets/";
        this.localFileDirectorySave = localFileDirectorySave + "\\";
        this.localFileDirectoryTmp = this.localFileDirectorySave +"tmpfilesSVGtoPNG\\";
        this.localFileDirectorySVG = localFileDirectorySVG;
        this.localFileDirectoryTmpAbsolut = Gdx.files.local(this.localFileDirectoryTmp).file().getAbsolutePath();//"GameStorage\\tmpfiles\\";
        this.localFileDirectorySaveAbsolut = Gdx.files.local(this.localFileDirectorySave).file().getAbsolutePath();
        this.localFileDirectorySVGAbsolut = Gdx.files.local(this.localFileDirectorySVG).file().getAbsolutePath();

    }


    public void transformSVGtoPNG(String pngFileName, String svgFileName){
        //String svg_URI_input = "C:/Users/Dave/Desktop/libGDX/IconStore/Arrow.svg";//Paths.get("chessboard.svg").toUri().toURL().toString();
        TranscoderInput input_svg_image;

        //Step-2: Define OutputStream to PNG Image and attach to TranscoderOutput
        //OutputStream png_ostream;
        try {
            //String svg_URI_input = "C:/Users/Dave/Desktop/libGDX/IconStore/Arrow.svg"; //Paths.get("chessboard.svg").toUri().toURL().toString();
            //System.out.println(localFileDirectorySVGAbsolut+"\\"+svgFileName);



            //FileHandle file = Gdx.files.local("myFile.txt");

            //PNGS have to be written in the Android Internal Store (which in not equal to the meaning of internal in libgdx), which is called "localStorage" in Libgdx
            String localFolderToSavePNG=null;

            if (Gdx.files.isLocalStorageAvailable()) {
                localFolderToSavePNG = Gdx.files.getLocalStoragePath();
            }

            System.out.println("Save to:" + localFolderToSavePNG);

            //The SVGs are stored internally (read only)
            String internalFolderFromSVG=null;
            //File svgFile = Gdx.files.internal("xmlstore/"+svgFileName).file();//.path();
            //File initialFile = new File(localFileDirectorySVGAbsolut+"\\"+svgFileName);
            //InputStream istream = new FileInputStream(svgFile);

            InputStream istream = Gdx.files.internal("xmlstore/"+svgFileName).read();


            System.out.println("Take from:" + internalFolderFromSVG);



            //ContextWrapper cw = new ContextWrapper();

            //ContextWrapper cw = new ContextWrapper(getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            //File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            // Create imageDir
            //File mypath=new File(directory,"profile.jpg");


            System.out.println("OUTPUT PATH:" + localFileDirectorySVGAbsolut+"\\"+svgFileName);

            String svg_URI_input = "Test";//Paths.get(localFileDirectorySVGAbsolut+"\\"+svgFileName).toUri().toURL().toString();
            //System.out.println("OUTPUT PATH:"+svg_URI_input);

            //input_svg_image = new TranscoderInput(svg_URI_input);//new FileInputStream("C:/Users/Dave/Desktop/libGDX/IconStore/Arrow.svg"));
            input_svg_image = new TranscoderInput(istream);
            OutputStream png_ostream;

            System.out.println("ENDREACHED");

            //System.out.println(this.localFileDirectoryTmp);
            //System.out.println("EXISTS:"+Gdx.files.local(this.localFileDirectoryTmp).exists());

            if(!Gdx.files.local(this.localFileDirectoryTmp).exists()){
                FileHandle file = Gdx.files.local(localFileDirectoryTmp+"\\placeholder.txt");
                file.writeString("placeholder", false);
            }
            //System.out.println(Gdx.files.local(this.localFileDirectorySVGAbsolut).file().getAbsolutePath());
            //System.out.println(Gdx.files.local(this.localFileDirectorySVGAbsolut).file().exists());
            //System.out.println("BeginTested");

            png_ostream = new FileOutputStream(localFileDirectoryTmpAbsolut+"\\"+pngFileName);
            TranscoderOutput output_png_image = new TranscoderOutput(png_ostream);
            // Step-3: Create PNGTranscoder and define hints if required
            PNGTranscoder my_converter = new PNGTranscoder();
            my_converter.addTranscodingHint(PNGTranscoder.KEY_WIDTH, iconWidth); //64px
            my_converter.addTranscodingHint(PNGTranscoder.KEY_HEIGHT, iconHeight); //32px

            // Step-4: Convert and Write output
            my_converter.transcode(input_svg_image, output_png_image);
            // Step 5- close / flush Output Stream
            png_ostream.flush();
            png_ostream.close();
            //System.out.println("EndReached");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (TranscoderException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }



}
