package com.towerdefense.factorycollection;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Dave on 19/06/2017.
 */

public abstract class AbstractVecToRasterFactory {

    public abstract void initializeVecToRasterFactory(String localFileDirectorySVG,String localFileDirectorySave); //don't use the directories for the Android version

    public abstract void transformSVGtoPNG(String pngFileName, String svgFileName);

    //public abstract Texture createTexture(int width, int height);

}
