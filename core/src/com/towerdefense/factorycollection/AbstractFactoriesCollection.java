package com.towerdefense.factorycollection;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Dave on 18/06/2017.
 */

public abstract class AbstractFactoriesCollection {

    public AbstractFactoriesParameter abstractFactoriesParameter;
    public AbstractVecToRasterFactory abstractVecToRasterFactory;

    public abstract void init();


}
