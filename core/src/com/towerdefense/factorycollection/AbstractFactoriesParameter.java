package com.towerdefense.factorycollection;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Dave on 23/06/2017.
 */

public abstract class AbstractFactoriesParameter {

    SpriteBatch batch;

    public abstract  SpriteBatch getBatch();
    public abstract void setBatch(SpriteBatch batch);
}
