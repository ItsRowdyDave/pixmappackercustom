package com.towerdefense.androidsvgconverter;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;

import java.io.IOException;

/**
 * Created by Dave on 21/06/2017.
 */

public class AndroidSVGConverter {

    AssetManager assetManager;

    public AndroidSVGConverter(AssetManager assetManager){

        this.assetManager = assetManager;

    }

    public Bitmap createAndroidBitmap(String pathFileName, int width, int height){

        return svgToBitmap(assetManager,pathFileName,width,height);

    }


    private Bitmap svgToBitmap(AssetManager assetManager, String pathFileName,int width, int height) {
        try {
            //size = (int)(size*res.getDisplayMetrics().density);
            //SVG svg = SVG.getFromResource(getApplicationContext(), resource);
            SVG svg = SVG.getFromAsset(assetManager, pathFileName);//"xmlstore/arrow.svg");

            Bitmap bmp;

            bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(bmp);

            svg.renderToCanvas(canvas);

            return bmp;

        } catch (SVGParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
