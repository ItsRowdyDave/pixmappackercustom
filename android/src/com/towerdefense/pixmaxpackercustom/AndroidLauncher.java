package com.towerdefense.pixmaxpackercustom;

import android.os.Bundle;
import android.os.Environment;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.towerdefense.factorycollection.AndroidFactoriesCollection;
import com.towerdefense.factorycollection.AndroidFactoriesParameter;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        //Here we set Parameter for the AndroidFactoryCollection
        AndroidFactoriesParameter androidFactoriesParameter = new AndroidFactoriesParameter();
        androidFactoriesParameter.setAssetManager(getAssets());

		//here we create the AndroidFactoryCollection
		AndroidFactoriesCollection androidFactoriesCollection = new AndroidFactoriesCollection(androidFactoriesParameter);


		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new PixmapPackerCustom(androidFactoriesCollection), config);

	}
}
