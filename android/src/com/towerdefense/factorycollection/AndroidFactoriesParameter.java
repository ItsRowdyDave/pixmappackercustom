package com.towerdefense.factorycollection;

import android.content.res.AssetManager;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Dave on 21/06/2017.
 */

public class AndroidFactoriesParameter extends  AbstractFactoriesParameter{

    AssetManager assetManager;

    public void setAssetManager(AssetManager assetManager) {
        this.assetManager = assetManager;
    }

    public AssetManager getAssetManager() {

        return assetManager;
    }


    @Override
    public SpriteBatch getBatch() {
        return batch;
    }

    @Override
    public void setBatch(SpriteBatch batch) {
        this.batch = batch;
    }
}
