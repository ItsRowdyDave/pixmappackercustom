package com.towerdefense.factorycollection;

import com.towerdefense.buttoncreator.AndroidTransformerVecToRaster;

/**
 * Created by Dave on 21/06/2017.
 */

public class AndroidFactoriesCollection extends AbstractFactoriesCollection {

    AndroidFactoriesParameter androidFactoriesParameter;

    public AndroidFactoriesCollection(AndroidFactoriesParameter androidFactoriesParameter){
        this.androidFactoriesParameter = androidFactoriesParameter;
    }

    @Override
    public void init() {
        System.out.println("AndroidCall");

        abstractVecToRasterFactory = new AndroidTransformerVecToRaster(androidFactoriesParameter.getAssetManager());

    }


}
