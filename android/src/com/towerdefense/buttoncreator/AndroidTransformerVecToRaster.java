package com.towerdefense.buttoncreator;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.os.Environment;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;
import com.towerdefense.androidsvgconverter.AndroidSVGConverter;
import com.towerdefense.factorycollection.AbstractVecToRasterFactory;
import com.towerdefense.pixmaxpackercustom.AndroidLauncher;

import java.io.IOException;

// Import the texture packer

/**
 * Created by Dave on 21/06/2017.
 */

public class AndroidTransformerVecToRaster extends AbstractVecToRasterFactory {

    AssetManager assetManager;

    public AndroidTransformerVecToRaster(AssetManager assetManager){
        this.assetManager = assetManager;
    }

    @Override
    public void initializeVecToRasterFactory(String localFileDirectorySVG, String localFileDirectorySave) {

    }

    @Override
    public void transformSVGtoPNG(String pngFileName, String svgFileName) {
        System.out.println("TransformSVGCallToPNG");
        //Context applicationContext = getApplicationContext();

        // Read an SVG from the assets folder
        //SVG  svg = SVG.getFromAsset(getContext().getAssets(), filename);

        //AndroidSVGConverter androidSVGConverter = new AndroidSVGConverter(assetManager);

        Pixmap tmpPixmap = loadSVGtoPixmap("xmlstore/sys_one_plan_two.svg",600,600);

        System.out.println("FilehandleReached");
        FileHandle cached = Gdx.files.external(pngFileName);

        PixmapIO.writePNG(cached, tmpPixmap);

        tmpPixmap.dispose();


    }

    private Pixmap writeTextureToPixmap(Texture texture){


        System.out.print("Is Texturenull ");
        System.out.println(texture==null);

        //texture.getTextureData().prepare();
        //System.out.println("PreConsumption");
        Pixmap tmpPixmap = texture.getTextureData().consumePixmap();
        System.out.println("AfterConsumption");
        //texture.dispose();

        System.out.println(tmpPixmap==null);
        System.out.println("Height"+tmpPixmap.getHeight());
        System.out.println("Width"+tmpPixmap.getWidth());


        return tmpPixmap;
    }

    private Pixmap loadSVGtoPixmap (String svgLocalPathAndFileName, int pngWidth, int pngHeight) {
        try {
            SVG svg = SVG.getFromAsset(assetManager, svgLocalPathAndFileName);
            System.out.println("Dokuwidth:" +svg.getDocumentWidth());
            //if (svg.getDocumentWidth() != -1) { //This if statement does not work for all svg formats

                System.out.println("Creating ANDROID BITMAP");
                Bitmap bitmap = Bitmap.createBitmap(pngWidth,pngHeight,Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                svg.renderToCanvas(canvas);

                Pixmap pixmap = new Pixmap(bitmap.getWidth(), bitmap.getHeight(), Pixmap.Format.RGBA8888);
                // i wonder why the colors are correct
                bitmap.copyPixelsToBuffer(pixmap.getPixels());
                bitmap.recycle();
                return pixmap;
            //}
        } catch (SVGParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /*
    @Override
    public Texture createTexture(int bitmapWidth, int bitmapHeight) {
        //System.out.println("Reached1");
        AndroidSVGConverter androidSVGConverter = new AndroidSVGConverter(assetManager);

        //System.out.println("Reached2");
        final Bitmap bitmapStore = androidSVGConverter.createAndroidBitmap("xmlstore/arrow.svg",bitmapWidth, bitmapHeight);
        final Texture tex = new Texture(bitmapStore.getWidth(), bitmapStore.getHeight(), Pixmap.Format.RGBA8888);

        //System.out.println("Reached3");
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tex.getTextureObjectHandle());
                GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmapStore, 0);
                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
                bitmapStore.recycle();
                // now all android bitmap data is stored in tex (which is of the libgdx Texture class)
            }
        });

        return tex;

    }
    */


    //MUST be called in render
    public void createPNGfromTexture(Texture texture, int pngWidth, int pngHeight, SpriteBatch batch){

        Gdx.gl.glViewport(0, 0, pngWidth, pngHeight); //Very Important! to set the glViewport to the correct status

        Gdx.gl.glClearColor(1, 0, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        Matrix4 normalProjection = new Matrix4().setToOrtho2D(0, 0, pngWidth, pngHeight); //the dimension of the actual shown screen (without the black bars)

        FrameBuffer tmpFrameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, pngWidth, pngHeight, false);
        Viewport viewportComplete = new StretchViewport(pngWidth, pngHeight); //we pick arbitrary worlddimensions, we just want to ensure that the texture fills the whole screen
        viewportComplete.update(pngWidth,pngHeight); //This gives the actual pixel size

        Gdx.gl.glViewport(0, 0, pngWidth, pngHeight); //Very Important! to set the glViewport to the correct status

        //Gdx.gl.glClearColor(0, 0, 1, 1);
        //Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        viewportComplete.apply();
        tmpFrameBuffer.bind();

        Gdx.gl.glClearColor(1.0f, 1.0f, 1.0f, 0.5f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(normalProjection);

        batch.begin();

        batch.draw(texture,
                0.0f, 0.0f,
                pngWidth, pngHeight);


        batch.end();

        byte[] pixels = ScreenUtils.getFrameBufferPixels(0, 0, pngWidth, pngHeight, true);

        Pixmap pixmap = new Pixmap(pngWidth, pngHeight, Pixmap.Format.RGBA8888);
        BufferUtils.copy(pixels, 0, pixmap.getPixels(), pixels.length);
        PixmapIO.writePNG(Gdx.files.local("pngstore/arrowtest.png"), pixmap);
        pixmap.dispose();

        tmpFrameBuffer.unbind();  //Interestingly here the Framebuffer.unbind is called and not lightEffectFrameBuffer.unbind();

        tmpFrameBuffer.dispose();
    }


}
