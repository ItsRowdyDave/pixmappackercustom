package factorycollection;

import com.towerdefense.factorycollection.AbstractFactoriesCollection;

import buttoncreator.DesktopTransformerVecToRaster;

/**
 * Created by Dave on 21/06/2017.
 */

public class DesktopFactoriesCollection extends AbstractFactoriesCollection {

    @Override
    public void init() {
        System.out.println("DesktopCall");

        abstractVecToRasterFactory = new DesktopTransformerVecToRaster();
    }


}
