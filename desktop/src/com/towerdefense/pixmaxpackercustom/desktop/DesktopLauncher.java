package com.towerdefense.pixmaxpackercustom.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.towerdefense.pixmaxpackercustom.PixmapPackerCustom;

import factorycollection.DesktopFactoriesCollection;

public class DesktopLauncher {
	public static void main (String[] arg) {
		//System.out.println("DesktopEntry");

		DesktopFactoriesCollection desktopFactoriesCollection = new DesktopFactoriesCollection();

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new PixmapPackerCustom(desktopFactoriesCollection), config);
	}
}
