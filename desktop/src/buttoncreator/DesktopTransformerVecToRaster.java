package buttoncreator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.ScreenUtils;
import com.towerdefense.factorycollection.AbstractVecToRasterFactory;

import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Dave on 21/06/2017.
 */

public class DesktopTransformerVecToRaster extends AbstractVecToRasterFactory {



    //int fboWidth;
    //int fboHeight;

    float iconWidth=40f;
    float iconHeight=40f;

    //float iconWidth;
    //float iconHeight;

    //String directoryPath;
    String pngFileName;

    PixmapPacker pixmapPacker;
    Pixmap tmpPixmap;

    TextureAtlas iconTextureAtlas;
    //tmpPixmap = new Pixmap(fboWidth,fboHeight,Format.RGBA8888);

    int pixmapPackerWidth;
    int pixmapPackerHeight;

    String localFileDirectoryTmp;
    String localFileDirectorySave;
    String localFileDirectorySVG;
    String localFileDirectoryTmpAbsolut;
    String localFileDirectorySaveAbsolut;
    String localFileDirectorySVGAbsolut;


    @Override
    public void initializeVecToRasterFactory(String localFileDirectorySVG,String localFileDirectorySave){
        this.localFileDirectorySave = localFileDirectorySave + "\\";
        this.localFileDirectoryTmp = this.localFileDirectorySave +"tmpfilesSVGtoPNG\\";
        this.localFileDirectorySVG = localFileDirectorySVG;
        this.localFileDirectoryTmpAbsolut = Gdx.files.local(this.localFileDirectoryTmp).file().getAbsolutePath();//"GameStorage\\tmpfiles\\";
        this.localFileDirectorySaveAbsolut = Gdx.files.local(this.localFileDirectorySave).file().getAbsolutePath();
        this.localFileDirectorySVGAbsolut = Gdx.files.local(this.localFileDirectorySVG).file().getAbsolutePath();

    }

    private boolean isFileAlreadyExisting(String pngFileName){
        //File file = new File(directoryPath+pngFileName);
        //return(file.exists() && !file.isDirectory());
        return(Gdx.files.local(localFileDirectoryTmp+"\\"+pngFileName).exists());
    }

    public void transformSVGtoPNG(SpriteBatch batch, String pngFileName, String svgFileName){

        Texture tex = new Texture("badlogic.jpg");




        //tex.getTextureData().prepare();
        Pixmap tmpPixmap = writeTextureToPixmap(tex);

        /*
        FileHandle fileHandle = Gdx.files.local("pngstore/picture.png");//localFileDirectoryTmp + gameButton.getTextureRegionName()+buttonState + ".png");//new FileHandle("GameStorage\\tmpfiles\\Hallo.png",Files.FileType.External);
        System.out.println("PreWriting");
        System.out.println(tmpPixmap.getPixel(30,30));
        PixmapIO.writePNG(fileHandle, tmpPixmap);
        System.out.println("AfterWriting");
        */
    }

    @Override
    public Texture createTexture(int width, int height) {
        return null;
    }


    private Pixmap writeTextureToPixmap(Texture texture){


        SpriteBatch batch = new SpriteBatch();

        System.out.print("Is Texturenull ");
        System.out.println(texture==null);

        //-------------------------------
        int width=200;
        int height=200;
        Matrix4 normalProjection = new Matrix4().setToOrtho2D(0, 0, width,  height); //the dimension of the actual shown screen (without the black bars)

        FrameBuffer tmpFrameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, false);


        System.out.println("SpriteInit");

        Sprite sprite = new Sprite(texture);
        System.out.println(sprite.getHeight());


        tmpFrameBuffer.bind();

        Gdx.gl.glViewport(0, 0, width, height); //Very Important! to set the glViewport to the correct status

        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        batch.setProjectionMatrix(normalProjection);

        sprite.draw(batch);

        byte[] pixels = ScreenUtils.getFrameBufferPixels(0, 0, width, height, true);

        /*
        for(int i=1; i<100; i++){
            System.out.println("PixelLength" + pixels[i]);
        }
        System.out.println(pixels.length);
        */
        Pixmap pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);
        BufferUtils.copy(pixels, 0, pixmap.getPixels(), pixels.length);
        PixmapIO.writePNG(Gdx.files.local("pngstore/mypixmap.png"), pixmap);
        pixmap.dispose();

        batch.end();//viewport.getScreenX(),viewport.getScreenY(), viewport.getScreenWidth(),viewport.getScreenHeight());

        FrameBuffer.unbind();  //Interestingly here the Framebuffer.unbind is called and not lightEffectFrameBuffer.unbind();




        //-------------------------------

        //texture.getTextureData().prepare();
        //System.out.println("PreConsumption");
        //Pixmap tmpPixmap = texture.getTextureData().consumePixmap();
        //System.out.println("AfterConsumption");
        //texture.dispose();

        //System.out.println(tmpPixmap==null);
        //System.out.println(tmpPixmap.getHeight());

        return tmpPixmap;
    }

    /*
    public void transformSVGtoPNG(String pngFileName, String svgFileName){
        //String svg_URI_input = "C:/Users/Dave/Desktop/libGDX/IconStore/Arrow.svg";//Paths.get("chessboard.svg").toUri().toURL().toString();
        TranscoderInput input_svg_image;

        //Step-2: Define OutputStream to PNG Image and attach to TranscoderOutput
        //OutputStream png_ostream;
        try {
            //String svg_URI_input = "C:/Users/Dave/Desktop/libGDX/IconStore/Arrow.svg"; //Paths.get("chessboard.svg").toUri().toURL().toString();
            //System.out.println(localFileDirectorySVGAbsolut+"\\"+svgFileName);

            //------------
            //String svg_URI_input = Paths.get(localFileDirectorySVGAbsolut+"\\"+svgFileName).toUri().toURL().toString();
            //System.out.println(svg_URI_input);

            //input_svg_image = new TranscoderInput(svg_URI_input);//new FileInputStream("C:/Users/Dave/Desktop/libGDX/IconStore/Arrow.svg"));
            //-------------

            //System.out.println(localFileDirectorySVG+"\\"+svgFileName);

            InputStream istream = Gdx.files.internal(localFileDirectorySVG+"\\"+svgFileName).read();

            input_svg_image = new TranscoderInput(istream);
            OutputStream png_ostream;

            //System.out.println(this.localFileDirectoryTmp);
            //System.out.println("EXISTS:"+Gdx.files.local(this.localFileDirectoryTmp).exists());

            if(!Gdx.files.local(this.localFileDirectoryTmp).exists()){
                FileHandle file = Gdx.files.local(localFileDirectoryTmp+"\\placeholder.txt");
                file.writeString("placeholder", false);
            }
            //System.out.println(Gdx.files.local(this.localFileDirectorySVGAbsolut).file().getAbsolutePath());
            //System.out.println(Gdx.files.local(this.localFileDirectorySVGAbsolut).file().exists());
            //System.out.println("BeginTested");

            png_ostream = new FileOutputStream(localFileDirectoryTmpAbsolut+"\\"+pngFileName);
            TranscoderOutput output_png_image = new TranscoderOutput(png_ostream);
            // Step-3: Create PNGTranscoder and define hints if required
            PNGTranscoder my_converter = new PNGTranscoder();
            my_converter.addTranscodingHint(PNGTranscoder.KEY_WIDTH, iconWidth); //64px
            my_converter.addTranscodingHint(PNGTranscoder.KEY_HEIGHT, iconHeight); //32px

            // Step-4: Convert and Write output
            my_converter.transcode(input_svg_image, output_png_image);
            // Step 5- close / flush Output Stream
            png_ostream.flush();
            png_ostream.close();
            //System.out.println("EndReached");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (TranscoderException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    */


	/*
	private void calculatePixmapSize(List<GameButton> listThisStageButtons, int width, int height){
		int squareSize=0;
		int maxButtonWidth=0;

		int sizeZero = 512; //512*512
		int sizeZeroSquared = 262144;//512*512

		int sizeOne= 1024; //1024*1024
		int sizeOneSquared= 1048576; //1024*1024

		int sizeTwo =2048; //2048*2048
		int sizeTwoSquared =4194304; //2048*2048

		int sizeThree = 4096; //4096*4096
		int sizeThreeSquared = 16777216; //4096*4096


		for(GameButton gameButton:listThisStageButtons){
			if(gameButton.getGameButtonType() == GameButtonType.ICONBUTTON){
				squareSize = squareSize+gameButton.getButtonPixelWidth(width)*gameButton.getButtonPixelHeight(height);
				maxButtonWidth = Math.max(gameButton.getButtonPixelWidth(width),maxButtonWidth);
			}
		}

		squareSize = squareSize*1; //No ADjustment necessary here, (in contrast to the buttonCreater)

		if ( ((float)squareSize/(float)sizeZeroSquared)<=0.7f &&(float)maxButtonWidth*0.5f<sizeZero){
			pixmapPackerWidth=sizeZero;
			pixmapPackerHeight=sizeZero;
		} else if ( ((float)squareSize/(float)sizeOneSquared)<=0.7f &&(float)maxButtonWidth*0.5f<sizeOne){
			pixmapPackerWidth=sizeOne;
			pixmapPackerHeight=sizeOne;
		} else if ( ((float)squareSize/(float)sizeTwoSquared)<=0.7f &&(float)maxButtonWidth*0.5f<sizeTwo){
			pixmapPackerWidth=sizeTwo;
			pixmapPackerHeight=sizeTwo;
		} else if ( ((float)squareSize/(float)sizeThreeSquared)<=0.7f &&(float)maxButtonWidth*0.5f<sizeThree){
			pixmapPackerWidth=sizeThree;
			pixmapPackerHeight=sizeThree;
		} else {
			System.out.println("ERROR, Pixmap too big?");
		}




	}
	*/




}
